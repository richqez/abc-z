package xyz.code.sheep.abc_z;

import android.content.Context;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private  final ImageView alphabet_images[] = new ImageView[26] ;
    private MediaPlayer mMediaPlayer ;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mMediaPlayer = new MediaPlayer();
        mContext = this ;

        alphabet_images[0] = (ImageView) findViewById(R.id.img_a);
        alphabet_images[1] = (ImageView) findViewById(R.id.img_b);
        alphabet_images[2] = (ImageView) findViewById(R.id.img_c);
        alphabet_images[3] = (ImageView) findViewById(R.id.img_d);
        alphabet_images[4] = (ImageView) findViewById(R.id.img_e);
        alphabet_images[5] = (ImageView) findViewById(R.id.img_f);
        alphabet_images[6] = (ImageView) findViewById(R.id.img_g);
        alphabet_images[7] = (ImageView) findViewById(R.id.img_h);
        alphabet_images[8] = (ImageView) findViewById(R.id.img_i);
        alphabet_images[9] = (ImageView) findViewById(R.id.img_j);
        alphabet_images[10] = (ImageView) findViewById(R.id.img_k);
        alphabet_images[11] = (ImageView) findViewById(R.id.img_l);
        alphabet_images[12] = (ImageView) findViewById(R.id.img_m);
        alphabet_images[13] = (ImageView) findViewById(R.id.img_n);
        alphabet_images[14] = (ImageView) findViewById(R.id.img_o);
        alphabet_images[15] = (ImageView) findViewById(R.id.img_p);
        alphabet_images[16] = (ImageView) findViewById(R.id.img_q);
        alphabet_images[17] = (ImageView) findViewById(R.id.img_r);
        alphabet_images[18] = (ImageView) findViewById(R.id.img_s);
        alphabet_images[19] = (ImageView) findViewById(R.id.img_t);
        alphabet_images[20] = (ImageView) findViewById(R.id.img_u);
        alphabet_images[21] = (ImageView) findViewById(R.id.img_v);
        alphabet_images[22] = (ImageView) findViewById(R.id.img_w);
        alphabet_images[23] = (ImageView) findViewById(R.id.img_x);
        alphabet_images[24] = (ImageView) findViewById(R.id.img_y);
        alphabet_images[25] = (ImageView) findViewById(R.id.img_z);


        AlphabetClick alphabetClick = new AlphabetClick();

        for (int i=0;i<alphabet_images.length;i++){
            alphabet_images[i].setOnClickListener(alphabetClick);
        }

        Toast.makeText(mContext,"init complate!",Toast.LENGTH_SHORT).show();
        Toast.makeText(mContext,"Made by SheepCode.xyz",Toast.LENGTH_LONG).show();

    }

    private void playRaw(int rawId){
        if (mMediaPlayer !=null){
            mMediaPlayer.stop();
            mMediaPlayer.reset();
            mMediaPlayer.release();
        }
        mMediaPlayer = MediaPlayer.create(this,rawId);
        mMediaPlayer.start();
    }

    private class AlphabetClick implements View.OnClickListener{
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.img_a: playRaw(R.raw.a); break;
                case R.id.img_b: playRaw(R.raw.b); break;
                case R.id.img_c: playRaw(R.raw.c); break;
                case R.id.img_e: playRaw(R.raw.e); break;
                case R.id.img_f: playRaw(R.raw.f); break;
                case R.id.img_g: playRaw(R.raw.g); break;
                case R.id.img_h: playRaw(R.raw.h); break;
                case R.id.img_i: playRaw(R.raw.i); break;
                case R.id.img_j: playRaw(R.raw.j); break;
                case R.id.img_k: playRaw(R.raw.k); break;
                case R.id.img_l: playRaw(R.raw.l); break;
                case R.id.img_m: playRaw(R.raw.m); break;
                case R.id.img_n: playRaw(R.raw.n); break;
                case R.id.img_o: playRaw(R.raw.o); break;
                case R.id.img_q: playRaw(R.raw.q); break;
                case R.id.img_r: playRaw(R.raw.r); break;
                case R.id.img_s: playRaw(R.raw.s); break;
                case R.id.img_t: playRaw(R.raw.t); break;
                case R.id.img_u: playRaw(R.raw.u); break;
                case R.id.img_v: playRaw(R.raw.v); break;
                case R.id.img_w: playRaw(R.raw.w); break;
                case R.id.img_x: playRaw(R.raw.x); break;
                case R.id.img_y: playRaw(R.raw.y); break;
                case R.id.img_z: playRaw(R.raw.z); break;
            }
        }
    }



}
